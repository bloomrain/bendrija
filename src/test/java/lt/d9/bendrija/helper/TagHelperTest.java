package lt.d9.bendrija.helper;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TagHelperTest {
	private TagHelper tag;
	
	@Before
	public void setUp() {
		tag = new TagHelper();
	}

	@Test
	public void testStylesheet() {
		assertEquals(
			tag.stylesheet("path/to.css"), 
			"<link rel=\"stylesheet\" href=\"/bendrija/path/to.css\">"
		);
	}

	@Test
	public void testJavascript() {
		assertEquals(
			tag.javascript("path/to.js"), 
			"<script type=\"text/javascript\" src=\"/bendrija/path/to.js\"></script>"
		);
	}
	
	@Test
	public void testLinkTo() {
		assertEquals(
			tag.linkTo("Title", "path/to.html"), 
			"<a href=\"/bendrija/path/to.html\">Title</a>"
		);
	}

}
