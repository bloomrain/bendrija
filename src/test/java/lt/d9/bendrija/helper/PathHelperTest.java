package lt.d9.bendrija.helper;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class PathHelperTest {
	private PathHelper path;
	
	@Before
    public void setUp() {
        path = new PathHelper();
    }

	@Test
	public void testTo() {
		assertEquals(path.to("some/path"), "/bendrija/some/path");
	}
	
	@Test
	public void testToWithAbsolutePath() {
		assertEquals(path.to("/"), "/bendrija//");
	}

}
