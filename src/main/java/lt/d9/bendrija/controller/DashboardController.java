package lt.d9.bendrija.controller;
 
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import lt.d9.bendrija.helper.HelpersIncluder;
 
@Controller
public class DashboardController {
	String message = "Welcome to Spring MVC!";
 
	@RequestMapping("/")
	public String show(
			@RequestParam(value = "name", required = false, defaultValue = "World") String name,
			final ModelMap model) {
		
		model.addAttribute("message", message);
		model.addAttribute("name", name);
		return "welcome/show";
	}
//	
//	@RequestMapping(value = { "/", "/welcome**" })
//	public ModelAndView welcomePage() {
//
//		ModelAndView model = new ModelAndView();
//		model.addObject("title", "Spring Security Hello World");
//		model.addObject("message", "This is welcome page!");
//		model.setViewName("hello");
//		return model;
//
//	}

	@RequestMapping("/admin")
	public String adminPage(final ModelMap model) {
		
		System.out.println("in admin controller");

		model.addAttribute("title", "Spring Security Hello World");
		model.addAttribute("message", "This is protected page!");

		return "admin";
	}
}