package lt.d9.bendrija.helper;

import org.springframework.ui.ModelMap;

public class HelpersIncluder {
	private ModelMap model;
	
    private HelpersIncluder(ModelMap model) {
    	this.model = model;
    }
    
    public static void includeBaseHelpers(ModelMap model) {
    	HelpersIncluder includer = new HelpersIncluder(model);
    	
    	includer.includeTagHelpers();
    }
        
    public void includeTagHelpers() {
    	model.addAttribute("tag", new TagHelper());
    }
    
    
}