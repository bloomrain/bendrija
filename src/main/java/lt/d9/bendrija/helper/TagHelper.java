package lt.d9.bendrija.helper;

public class TagHelper {
	private final PathHelper path = new PathHelper();
	
	public String stylesheet(final String relativePath) {
		return "<link rel=\"stylesheet\" href=\"" + path.to(relativePath) + "\">";
	}
	
	public String javascript(final String relativePath) {
		return "<script type=\"text/javascript\" src=\"" + path.to(relativePath) + "\"></script>";
	}

	public String linkTo(String title, String relativePath) {
		return("<a href=\"" + path.to(relativePath) + "\">" + title +"</a>");
	}
}