package lt.d9.bendrija.helper;

public class PathHelper {
	private final String PREFIX = "/bendrija/";
	
	public String to(final String relativePath) {
		return PREFIX + relativePath;
	}
	
}