package lt.d9.bendrija.inseptor;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import lt.d9.bendrija.helper.TagHelper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;

public class DefaultHelpersInseptor extends HandlerInterceptorAdapter {
    @Override
    public void postHandle(final HttpServletRequest request,
            final HttpServletResponse response, final Object handler,
            final ModelAndView modelAndView) throws Exception {

        if (modelAndView != null) {
            modelAndView.getModelMap().addAttribute("tag", new TagHelper());
        }
    }

}
