package lt.d9.bendrija.dao;

import javax.sql.DataSource;
import org.springframework.jdbc.core.JdbcTemplate;

import lt.d9.bendrija.model.Community;
import java.util.List;

public class CommunityDAO {
	private JdbcTemplate jdbcTemplate;
	
	public CommunityDAO(DataSource dataSource) {
		jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
    public void saveOrUpdate(Community community) {
        // implementation details goes here...
    }
 
     public void delete(int id) {
        // implementation details goes here...
    }
 
    public List<Community> list() {
        // implementation details goes here...
    }
 
    public Community get(int id) {
        // implementation details goes here...
    }
}
